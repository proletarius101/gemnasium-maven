variables:
  REPORT_FILENAME: gl-dependency-scanning-report.json
  MAJOR: 2
  MAX_IMAGE_SIZE_BYTE: 1100000000
  SCAN_DURATION_MARGIN_PERCENT: 25

include:
  - https://gitlab.com/gitlab-org/security-products/ci-templates/raw/master/includes-dev/analyzer.yml

cache:
  paths:
    - .gradle/wrapper

shell check:
  image: koalaman/shellcheck-alpine:stable
  stage: pre-build
  before_script:
    - shellcheck --version
  script:
    - shellcheck config/*.sh config/.bashrc

shfmt:
  image: mvdan/shfmt:v3.1.0-alpine
  stage: pre-build
  before_script:
    - shfmt -version
  script:
    - shfmt -i 2 -ci -d config

.functional:
  extends: .qa-downstream-ds
  variables:
    DS_DEFAULT_ANALYZERS: "gemnasium-maven"
    DS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"

# test DS_EXCLUDED_PATHS to ensure that the given path is ignored and results in no vulnerabilities in the
# expected report
ds-excluded-paths-qa:
 extends: .functional
 variables:
   MAX_SCAN_DURATION_SECONDS: 53
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/no-vulnerabilities/$REPORT_FILENAME"
   DS_EXCLUDED_PATHS: "/pom.xml"
 trigger:
   project: gitlab-org/security-products/tests/java-maven

java-maven-qa:
 extends: .functional
 variables:
   MAX_SCAN_DURATION_SECONDS: 53
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven

java-maven-java-8-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 8
   MAVEN_CLI_OPTS: -Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8 -DskipTests --batch-mode
   MAX_SCAN_DURATION_SECONDS: 55
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven

java-maven-java-11-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 11
   MAVEN_CLI_OPTS: -Dmaven.compiler.source=11 -Dmaven.compiler.target=11 -DskipTests --batch-mode
   MAX_SCAN_DURATION_SECONDS: 115
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven

java-maven-java-13-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 13
   MAVEN_CLI_OPTS: -Dmaven.compiler.source=13 -Dmaven.compiler.target=13 -DskipTests --batch-mode
   MAX_SCAN_DURATION_SECONDS: 55
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven

java-maven-java-14-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 14
   MAVEN_CLI_OPTS: -Dmaven.compiler.source=14 -Dmaven.compiler.target=14 -DskipTests --batch-mode
   MAX_SCAN_DURATION_SECONDS: 53
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven

java-maven-offline-qa:
 extends: .functional
 variables:
   MAX_SCAN_DURATION_SECONDS: 53
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven
   branch: offline-FREEZE

java-maven-multimodule-qa:
 extends: .functional
 variables:
   MAX_SCAN_DURATION_SECONDS: 55
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven-multimodules/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-maven-multimodules

java-gradle-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 50
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/java-gradle

java-gradle-8-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 8
   MAX_SCAN_DURATION_SECONDS: 50
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-gradle

java-gradle-11-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 11
   MAX_SCAN_DURATION_SECONDS: 50
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-gradle

java-gradle-13-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 13
   MAX_SCAN_DURATION_SECONDS: 50
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-gradle
   branch: java-14 # same as java-13

java-gradle-14-qa:
 extends: .functional
 variables:
   DS_JAVA_VERSION: 14
   MAX_SCAN_DURATION_SECONDS: 50
   DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-gradle
   branch: java-14

java-gradle-kotlin-dsl-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 85
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle-kotlin-dsl/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/java-gradle-kotlin-dsl

scala-sbt-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

scala-sbt-java-8-qa:
  extends: .functional
  variables:
    DS_JAVA_VERSION: 8
    MAX_SCAN_DURATION_SECONDS: 110
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

scala-sbt-java-11-qa:
  extends: .functional
  variables:
    DS_JAVA_VERSION: 11
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

java-gradle-multimodule-qa:
 extends: .functional
 variables:
  MAX_SCAN_DURATION_SECONDS: 35
  DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle-multimodules/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-gradle-multimodules

java-gradle-no-root-dependencies-qa:
 extends: .functional
 variables:
  MAX_SCAN_DURATION_SECONDS: 35
  DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-gradle-multimodules/no-root-dependencies/$REPORT_FILENAME"
 trigger:
   project: gitlab-org/security-products/tests/java-gradle-multimodules
   branch: no-root-dependencies-FREEZE

scala-sbt-1-0:
  extends: .functional
  variables:
    SBT_CLI_OPTS: "-Dsbt.version=1.0.4"
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

scala-sbt-1-1:
  extends: .functional
  variables:
    SBT_CLI_OPTS: "-Dsbt.version=1.1.6"
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

scala-sbt-1-2:
  extends: .functional
  variables:
    SBT_CLI_OPTS: "-Dsbt.version=1.2.8"
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

scala-sbt-1-3:
  extends: .functional
  variables:
    SBT_CLI_OPTS: "-Dsbt.version=1.3.12"
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt

scala-sbt-1-4:
  extends: .functional
  variables:
    SBT_CLI_OPTS: "-Dsbt.version=1.4.6"
    MAX_SCAN_DURATION_SECONDS: 120
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/scala-sbt/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/scala-sbt
