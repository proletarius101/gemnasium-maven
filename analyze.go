package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/keystore"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"

	// vrange
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/semver"

	// parser plugins
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/sbt"

	// builder plugins
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/gradle"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/maven"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder/sbt"
)

var (
	errNoInputFile = errors.New("no supported project file found (e.g. pom.xml, build.gradle, build.sbt)")
	errNoBuilder   = errors.New("no builder for requirements file")
)

func analyzeFlags() []cli.Flag {
	flags := []cli.Flag{}
	flags = append(flags, builder.Flags()...)
	flags = append(flags, scanner.Flags()...)
	flags = append(flags, vrange.Flags()...)
	return flags
}

func analyze(c *cli.Context, dir string) (io.ReadCloser, error) {
	// dependency scanner
	scan, err := scanner.NewScanner(c)
	if err != nil {
		return nil, err
	}

	// configure version range resolvers
	if err := vrange.Configure(c); err != nil {
		return nil, err
	}

	// configure CA certificates
	if err := keystore.Update(c); err != nil {
		return nil, err
	}

	// configure builders
	if err := builder.Configure(c); err != nil {
		return nil, err
	}

	// find supported projects
	log.Debugf("Finding compatible projects in %s", dir)
	finder := finder.NewFinder(c, finder.PresetGemnasiumMaven)
	finder.MaxDepth.Valid = false // disable max depth so that sub-modules are never skipped
	projects, err := finder.FindProjects(dir)
	if err != nil {
		return nil, err
	}

	// build projects
	for i, p := range projects {
		reqFile, found := p.RequirementsFile()
		if !found {
			continue
		}
		inputPath := filepath.Join(dir, p.FilePath(reqFile))
		log.Debugf("Exporting dependencies for %s", inputPath)
		pkgManager := p.PackageManager.Name
		b := builder.Lookup(pkgManager)
		if b == nil {
			log.Errorf("No builder for package manager %s", pkgManager)
			return nil, errNoBuilder
		}
		outputPath, err := b.Build(inputPath)
		if err == builder.ErrNoDependencies {
			// skip if builder finds no dependencies
			continue
		} else if err != nil {
			return nil, err
		}
		projects[i].AddScannableFilename(filepath.Base(outputPath))
	}

	// scan projects
	result, err := scan.ScanProjects(dir, projects)
	if err != nil {
		return nil, err
	}

	// return affected sources
	var output bytes.Buffer
	enc := json.NewEncoder(&output)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", "  ")
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}
