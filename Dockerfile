ARG GO_VERSION=1.15
FROM golang:$GO_VERSION-alpine AS analyzer-builder
# Build a static binary
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM debian:stable-slim

ENV VRANGE_DIR="/vrange"
ARG GEMNASIUM_VRANGE_BRANCH="v2.3.0"
ARG GEMNASIUM_VRANGE_REPO="https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium"
ARG GEMNASIUM_VRANGE_URL="$GEMNASIUM_VRANGE_REPO/raw/$GEMNASIUM_VRANGE_BRANCH/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ADD utils/maven-plugin-builder /maven-plugin-builder
ADD gemnasium-gradle-plugin-init.gradle /
ENV GRADLE_PLUGIN_INIT_PATH="/gemnasium-gradle-plugin-init.gradle"
ADD utils/gradle-plugin-builder /gradle-plugin-builder

ADD utils/sbt-dependency-graph-plugin /sbt-dependency-graph-plugin
ENV DEP_GRAPH_PLUGIN="addSbtPlugin(\"net.virtual-void\" % \"sbt-dependency-graph\" % \"0.10.0-RC1\" from \"file:///sbt-dependency-graph-plugin/sbt-dependency-graph-0.10.0-RC1.jar\")"

ENV ASDF_DATA_DIR="/opt/asdf"
ENV HOME=/gemnasium-maven
ENV TERM="xterm"
WORKDIR $HOME
COPY config/install.sh /root
COPY config/.tool-versions $HOME
COPY config/.bashrc $HOME
RUN bash /root/install.sh

# Install analyzer
COPY --from=analyzer-builder --chown=root:root /go/src/app/analyzer /analyzer-binary
COPY analyzer-wrapper /analyzer

CMD ["/analyzer", "run"]
