package keystore

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"os/exec"
	"testing"
)

func TestImportCmd(t *testing.T) {
	var tcs = []struct {
		name         string
		javaHome     string
		wantKeystore string
	}{
		{
			"java8",
			"/opt/asdf/installs/java/adoptopenjdk-8.0.252+9.1",
			"/opt/asdf/installs/java/adoptopenjdk-8.0.252+9.1/jre/lib/security/cacerts",
		},

		{
			"java11",
			"/opt/asdf/installs/java/adoptopenjdk-11.0.7+10.1",
			"/opt/asdf/installs/java/adoptopenjdk-11.0.7+10.1/lib/security/cacerts",
		},
		{
			"java13",
			"/opt/asdf/installs/java/adoptopenjdk-13.0.2+8.1",
			"/opt/asdf/installs/java/adoptopenjdk-13.0.2+8.1/lib/security/cacerts",
		},
		{
			"java14",
			"/opt/asdf/installs/java/adoptopenjdk-14.0.1+7.1",
			"/opt/asdf/installs/java/adoptopenjdk-14.0.1+7.1/lib/security/cacerts",
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			expected := exec.Command(
				"keytool",
				"-importcert",
				"-alias", "custom",
				"-file", cacert.DefaultBundlePath,
				"-trustcacerts",
				"-noprompt",
				"-storepass", "changeit",
				"-keystore", tc.wantKeystore,
			)

			assert := assert.New(t)
			assert.Equal(expected, importCmd(tc.javaHome))
		})
	}
}
