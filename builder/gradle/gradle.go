package gradle

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
)

const (
	pathGemnasiumPluginOutput = "gradle-dependencies.json"
	pathDefaultGradleBinary   = "/opt/asdf/shims/gradle"

	flagGradleOpts       = "gradle-opts"
	flagGradleInitScript = "gradle-init-script"
)

var errNoGradleWrapper = errors.New("cannot access gradle wrapper")

// Builder generates dependency lists for gradle projects
type Builder struct {
	GradleOpts       string
	GradleInitScript string
}

// Flags returns the CLI flags that configure the gradle command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagGradleOpts,
			Usage:   "Optional CLI arguments for the gradle dependency dump command",
			Value:   "",
			EnvVars: []string{"GRADLE_CLI_OPTS"},
		},
		&cli.StringFlag{
			Name:    flagGradleInitScript,
			Usage:   "Optional CLI argument pointing to the init script for gemnasium-gradle-plugin",
			Value:   "gemnasium-gradle-plugin-init.gradle",
			EnvVars: []string{"GRADLE_PLUGIN_INIT_PATH"},
		},
	}
}

// Configure configures the gradle command
func (b *Builder) Configure(c *cli.Context) error {
	b.GradleOpts = c.String(flagGradleOpts)
	b.GradleInitScript = c.String(flagGradleInitScript)
	return nil
}

// Build generates a dependency list for Gradle build script or wrapper
func (b Builder) Build(input string) (string, error) {
	// skip if plugin output is already present
	output := filepath.Join(filepath.Dir(input), pathGemnasiumPluginOutput)
	if _, err := os.Stat(output); err == nil {
		return output, nil
	}

	err := b.listDeps(input)
	return output, err
}

// listDeps exports the dependency list using gemnasium-gradle-plugin
func (b Builder) listDeps(input string) error {
	// find gradle wrapper
	dir := filepath.Dir(input)
	gradlewPath := filepath.Join(dir, "gradlew")
	if _, err := os.Stat(gradlewPath); err != nil {
		gradlewPath = pathDefaultGradleBinary
	}

	// run gemnasiumDumpDependencies gradle task
	args := strings.Fields(b.GradleOpts)
	args = append(args, "--init-script", b.GradleInitScript, "gemnasiumDumpDependencies")
	cmd := exec.Command(gradlewPath, args...)
	cmd.Dir = dir
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if strings.Contains(string(output), "No dependencies") {
		return builder.ErrNoDependencies
	}
	return err
}

func init() {
	builder.Register("gradle", &Builder{})
}
