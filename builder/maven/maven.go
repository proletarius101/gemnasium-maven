package maven

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
)

const (
	flagMavenOpts = "maven-opts"

	pathGemnasiumPluginOutput = "gemnasium-maven-plugin.json"

	mavenPluginVersion = "0.4.0"
)

// Builder generates dependency lists for maven projects
type Builder struct {
	MavenOpts string
}

// Flags returns the CLI flags that configure the mvn command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagMavenOpts,
			Usage:   "Optional CLI arguments for the maven install command",
			Value:   "-DskipTests --batch-mode",
			EnvVars: []string{"MAVEN_CLI_OPTS"},
		},
	}
}

// Configure configures the mvn command
func (b *Builder) Configure(c *cli.Context) error {
	b.MavenOpts = c.String(flagMavenOpts)
	return nil
}

// Build generates a dependency list for maven projects
func (b Builder) Build(input string) (string, error) {
	// skip if plugin output is already present
	output := filepath.Join(filepath.Dir(input), pathGemnasiumPluginOutput)
	if _, err := os.Stat(output); err == nil {
		return output, nil
	}

	// install maven dependencies
	if err := b.installDeps(input); err != nil {
		return "", err
	}

	err := b.listDeps(input)
	return output, err
}

// installDeps installs the maven dependencies
func (b Builder) installDeps(input string) error {
	args := []string{"install"}
	args = append(args, strings.Fields(b.MavenOpts)...)
	cmd := exec.Command("mvn", args...)
	cmd.Dir = filepath.Dir(input)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

// listDeps exports the dependency list using the gemnasium-maven-plugin
func (b Builder) listDeps(input string) error {
	task := "com.gemnasium:gemnasium-maven-plugin:" + mavenPluginVersion + ":dump-dependencies"
	args := []string{task}
	args = append(args, strings.Fields(b.MavenOpts)...)
	cmd := exec.Command("mvn", args...)
	cmd.Dir = filepath.Dir(input)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

func init() {
	builder.Register("maven", &Builder{})
}
