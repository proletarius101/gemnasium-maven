#!/bin/bash
set -euo pipefail

export DEBIAN_FRONTEND=noninteractive

# grant_write_perm gives write access to root group (gid 0)
# to enable OpenShift support
grant_write_perm() {
  chmod -R g+w "$1"
}

# give write access to CA certificates (OpenShift)
mkdir -p /etc/ssl/certs/
touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem
grant_write_perm /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

# give write access to /etc/gitconfig where cacert writes sslCAInfo (OpenShift)
touch /etc/gitconfig
grant_write_perm /etc/gitconfig

echo ["$(date "+%H:%M:%S")"] "==> Installing packages…"
apt-get clean
apt-get update -q
apt-get install -y --no-install-recommends \
  bsdmainutils \
  git \
  jq \
  curl \
  ca-certificates \
  zstd \
  unzip
apt-get upgrade -y

echo ["$(date "+%H:%M:%S")"] "==> Installing asdf…"
mkdir -p "$ASDF_DATA_DIR"
git clone https://github.com/asdf-vm/asdf.git "$ASDF_DATA_DIR"
cd "$ASDF_DATA_DIR"
git checkout "$(git describe --abbrev=0 --tags)"

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/asdf.sh
asdf plugin add java
# TODO: Use official asdf-maven plugin in gemnasium-maven
# See https://gitlab.com/gitlab-org/gitlab/-/issues/224142
asdf plugin add maven https://github.com/adamcohen/asdf-maven
asdf plugin add sbt
asdf plugin add gradle

asdf install
asdf reshim
asdf current

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash

mkdir -p "$VRANGE_DIR"/semver
curl -o "$VRANGE_DIR"/semver/vrange-linux "$GEMNASIUM_VRANGE_URL"/semver/vrange-linux
chmod +x "$VRANGE_DIR"/semver/vrange-linux

echo ["$(date "+%H:%M:%S")"] "==> Cloning gemnasium-db"
git clone --branch "$GEMNASIUM_DB_REF_NAME" "$GEMNASIUM_DB_REMOTE_URL" "$GEMNASIUM_DB_LOCAL_PATH"
grant_write_perm "$GEMNASIUM_DB_LOCAL_PATH"

echo ["$(date "+%H:%M:%S")"] "==> Installing maven plugin"
mvn -q -f /maven-plugin-builder clean install

echo ["$(date "+%H:%M:%S")"] "==> Adding sbt-dependency-graph plugin"
sbt sbtVersion
mkdir -p "$HOME/.sbt/1.0/plugins"
echo "$DEP_GRAPH_PLUGIN" >"$HOME/.sbt/1.0/plugins/plugins.sbt"
grant_write_perm "$HOME/.sbt"

# also write sbt plugins configuration in HOME directory of root user
# because this is where it's read from when user is root
mkdir -p "/root/.sbt/1.0/plugins"
echo "$DEP_GRAPH_PLUGIN" >"/root/.sbt/1.0/plugins/plugins.sbt"

echo ["$(date "+%H:%M:%S")"] "==> Installing gradle plugin"
cd /
gradle -p gradle-plugin-builder shadowJar
rm -rf /root/.gradle

echo ["$(date "+%H:%M:%S")"] "==> Beginning cleanup…"
rm -fr /tmp
mkdir -p /tmp
chmod 777 /tmp
chmod +t /tmp

rm -fr "$ASDF_DATA_DIR/docs" \
  "$ASDF_DATA_DIR"/installs/golang/**/go/test \
  "$ASDF_DATA_DIR"/installs/java/**/demo \
  "$ASDF_DATA_DIR"/installs/java/**/man \
  "$ASDF_DATA_DIR"/installs/java/**/sample \
  "$ASDF_DATA_DIR"/installs/python/**/lib/**/test \
  "$ASDF_DATA_DIR"/installs/ruby/**/lib/ruby/gems/**/cache \
  "$ASDF_DATA_DIR"/installs/**/**/share \
  "$ASDF_DATA_DIR"/test \
  "$HOME"/.config/configstore/update-notifier-npm.json \
  "$HOME"/.config/pip/selfcheck.json \
  "$HOME"/.gem \
  "$HOME"/.npm \
  "$HOME"/.wget-hsts \
  /etc/apache2/* \
  /etc/bash_completion.d/* \
  /etc/calendar/* \
  /etc/cron.d/* \
  /etc/cron.daily/* \
  /etc/emacs/* \
  /etc/fonts/* \
  /etc/ldap/* \
  /etc/mysql/* \
  /etc/php/*/apache2/* \
  /etc/profile.d/* \
  /etc/systemd/* \
  /etc/X11/* \
  /lib/systemd/* \
  /usr/lib/apache2/* \
  /usr/lib/systemd/* \
  /usr/lib/valgrid/* \
  /usr/share/applications/* \
  /usr/share/apps/* \
  /usr/share/bash-completion/* \
  /usr/share/calendar/* \
  /usr/share/doc-base/* \
  /usr/share/emacs/* \
  /usr/share/fontconfig/* \
  /usr/share/fonts/* \
  /usr/share/gtk-doc/* \
  /usr/share/icons/* \
  /usr/share/menu/* \
  /usr/share/pixmaps/* \
  /usr/share/themes/* \
  /usr/share/X11/* \
  /usr/share/zsh/* \
  /var/cache/* \
  /var/cache/apt/archives/ \
  /var/lib/apt/lists/* \
  /var/lib/systemd/* \
  /var/log/*

grant_write_perm "/opt"

echo ["$(date "+%H:%M:%S")"] "==> Starting compression…"
zstd_command="/usr/bin/zstd -19 -T0"
cd /opt
tar --use-compress-program "$zstd_command" -cf /opt/asdf.tar.zst asdf &

wait
rm -fr \
  /opt/asdf/
grant_write_perm /opt/asdf.tar.zst

grant_write_perm "$HOME"

echo ["$(date "+%H:%M:%S")"] "==> Done"
